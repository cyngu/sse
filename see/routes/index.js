var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});


let lat = -90;
const lon = 3.8833;

router.get('/events', (req, res) => {
  res.setHeader('Content-Type', 'text/event-stream');
  res.setHeader('Cache-Control', 'no-cache');
  res.setHeader('Connection', 'keep-alive');

  const sendEvent = async () => {
    lat = lat >= 90 ? -90 : lat += 1;

    const data = (await fetch(`${process.env.APP_URL}/weather?lat=${lat}&lon=${lon}&appid=${process.env.APP_ID}`))
    const temp = await data.json()

    res.write(`data: ${JSON.stringify({value: temp.main.temp - 273.15, lat: lat })}\n\n`);
  };

  const intervalId = setInterval(sendEvent, 500);

  req.on('close', () => {
    clearInterval(intervalId);
  });
});

module.exports = router;
